FabFire Provisioning Tool
===
Tool for provisioning new DESFire EV2 cards for use with the FabAccess card system. Using the cards requires a configured instance of the [fabfire adapter](https://gitlab.com/fabinfra/fabaccess/fabfire_adapter).

# Installation and usage
You can find all details about FabFire Provisioning Tool at [docs.fab-access.org](https://docs.fab-access.org/books/plugins-aktoren-initiatoren/page/fabfire-tools#bkmrk-fabfire-provisioning).